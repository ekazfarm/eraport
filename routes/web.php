<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


// Route::get('/', function() {
//     return view('layouts.master');
// });


Auth::routes();
Route::get('logout', 'HomeController@logout');

Route::group(['middleware' => ['auth','checkRole:Admin,Siswa,Superadmin']], function () {

    Route::get('/dashboard', 'PageController@dashboard');

    Route::get('/siswa/{siswa_id}/profile', 'SiswaController@profile');

    Route::get('/siswa/{siswa_id}/edit', 'SiswaController@edit');

    Route::put('siswa/{siswa_id}', 'SiswaController@update');
});


Route::group(['middleware' => ['auth','checkRole:Superadmin,Admin']], function () {

    

Route::get('/siswa', 'SiswaController@siswa');

Route::get('/siswa/{siswa}/{mapel}/deletnilai', 'SiswaController@deletnilai');

Route::get('/siswa/input', 'SiswaController@input');

Route::post('/siswa/store', 'SiswaController@store');

Route::put('siswa/{siswa_id}', 'SiswaController@update');

Route::delete('/siswa/{siswa_id}', 'SiswaController@destroy');

//Guru Route

Route::get('/guru', 'GuruController@index');

Route::get('/guru/input', 'GuruController@input');

Route::post('/guru/store', 'GuruController@store');

Route::get('/guru/{guru_id}/profile', 'GuruController@profile');

Route::get('/guru/{guru_id}/edit', 'GuruController@edit');

Route::put('/guru/{guru_id}', 'GuruController@update');

Route::delete('/guru/{guru_id}', 'GuruController@destroy');

//Mata Pelajaran Route

Route::get('/mapel', 'MapelController@index');

Route::get('/mapel/input', 'MapelController@input');

Route::post('/mapel/store', 'MapelController@store');

Route::get('mapel/{mapel_id}/edit', 'MapelController@edit');

Route::put('/mapel/{mapel_id}', 'MapelController@update');

Route::delete('/mapel/{mapel_id}', 'MapelController@destroy');


});