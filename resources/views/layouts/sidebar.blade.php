<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <div class="sidebar-brand-wrapper d-none d-lg-flex align-items-center justify-content-center fixed-top">
      <a class="sidebar-brand brand-logo" href="index.html"><img src="{{asset('corono/assets/images/logo.svg') }}" alt="logo" /></a>
      <a class="sidebar-brand brand-logo-mini" href="index.html"><img src="{{asset('corono/assets/images/logo-mini.svg') }}" alt="logo" /></a>
    </div>
    <ul class="nav">
      <li class="nav-item profile">
        <div class="profile-desc">
          <div class="profile-pic">
            <div class="count-indicator">
              @if (auth()->user()->role == 'Siswa')
              <img class="img-xs rounded-circle " src="{{ auth()->user()->siswa->getAvatar() }}" alt="">
              @elseif (auth()->user()->role == 'Admin')
              <img class="img-xs rounded-circle " src="{{ auth()->user()->guru->getAvatar() }}" alt="">
              @elseif (auth()->user()->role == 'SuperAdmin')
              <img class="img-xs rounded-circle " src="{{ auth()->user()->guru->getAvatar() }}" alt="">
              @endif
              <span class="count bg-success"></span>
            </div>
            <div class="profile-name">
              <h5 class="mb-0 font-weight-normal">{{ auth()->user()->email }}</h5>
            </div>
          </div>
          {{-- <a href="#" id="profile-dropdown" data-toggle="dropdown"><i class="mdi mdi-dots-vertical"></i></a> --}}
          <div class="dropdown-menu dropdown-menu-right sidebar-dropdown preview-list" aria-labelledby="profile-dropdown">
            {{-- <a href="" class="dropdown-item preview-item">
              <div class="preview-thumbnail">
                <div class="preview-icon bg-dark rounded-circle">
                  <i class="mdi mdi-settings text-primary"></i>
                </div>
              </div>
              <div class="preview-item-content">
                <p class="preview-subject ellipsis mb-1 text-small">Account settings</p>
              </div>
            </a> --}}
            <div class="dropdown-divider"></div>
            {{-- <a href="#" class="dropdown-item preview-item">
              <div class="preview-thumbnail">
                <div class="preview-icon bg-dark rounded-circle">
                  <i class="mdi mdi-onepassword  text-info"></i>
                </div>
              </div>
              <div class="preview-item-content">
                <p class="preview-subject ellipsis mb-1 text-small">Change Password</p>
              </div>
            </a> --}}
            <div class="dropdown-divider"></div>
            {{-- <a href="#" class="dropdown-item preview-item">
              <div class="preview-thumbnail">
                <div class="preview-icon bg-dark rounded-circle">
                  <i class="mdi mdi-calendar-today text-success"></i>
                </div>
              </div>
              <div class="preview-item-content">
                <p class="preview-subject ellipsis mb-1 text-small">To-do list</p>
              </div>
            </a> --}}
          </div>
        </div>
      </li>
      <li class="nav-item nav-category">
      </li>
      <li class="nav-item menu-items">
        <a class="nav-link" href="/dashboard">
          <span class="menu-icon">
            <i class="mdi mdi-speedometer"></i>
          </span>
          <span class="menu-title">Dashboard</span>
        </a>
      </li>
      @if (auth()->user()->role == 'Superadmin')
      <li class="nav-item menu-items">
        <a class="nav-link" href="/siswa">
          <span class="menu-icon">
            <i class="mdi mdi-account-multiple"></i>
          </span>
          <span class="menu-title">Siswa</span>
        </a>
      </li>
      @elseif (auth()->user()->role == 'Admin')
      <li class="nav-item menu-items">
        <a class="nav-link" href="/siswa">
          <span class="menu-icon">
            <i class="mdi mdi-account-multiple"></i>
          </span>
          <span class="menu-title">Siswa</span>
        </a>
      </li>
      @endif
      @if (auth()->user()->role == 'Superadmin')
      <li class="nav-item menu-items">
        <a class="nav-link" href="/guru">
          <span class="menu-icon">
            <i class="mdi mdi-account-multiple-outline"></i>
          </span>
          <span class="menu-title">Guru</span>
        </a>
      </li>
      @endif
      @if (auth()->user()->role == 'Superadmin')
      <li class="nav-item menu-items">
        <a class="nav-link"  href="/mapel" aria-expanded="false" aria-controls="ui-basic">
          <span class="menu-icon">
            <i class="mdi mdi-library-books"></i>
          </span>
          <span class="menu-title">Mata Pelajaran</span>
        </a>
      </li>
      @endif
      @if (auth()->user()->role == 'Admin')
      <li class="nav-item menu-items">
        <a class="nav-link"  href="/mapel" aria-expanded="false" aria-controls="ui-basic">
          <span class="menu-icon">
            <i class="mdi mdi-library-books"></i>
          </span>
          <span class="menu-title">Mata Pelajaran</span>
        </a>
      </li>
      @endif
      <li class="nav-item menu-items">
        <a class="nav-link collapsed" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
          <span class="menu-icon">
            <i class="mdi mdi-laptop"></i>
          </span>
          <span class="menu-title">Profile</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="ui-basic" style="">
          <ul class="nav flex-column sub-menu">
            @if (auth()->user()->role == 'Admin')
            <li class="nav-item"> <a class="nav-link" href="/guru/{{ auth()->user()->$guru->id }}/edit">Edit Profile</a></li>
            <li class="nav-item"> <a class="nav-link" href="/guru/{{ auth()->user()->$guru->id }}/profile">Profile</a></li>
            @endif
            @if (auth()->user()->role == 'Siswa')
            <li class="nav-item"> <a class="nav-link" href="/siswa/{{ auth()->user()->siswa->id }}/edit">Edit Profile</a></li>
            <li class="nav-item"> <a class="nav-link" href="/siswa/{{ auth()->user()->siswa->id }}/profile">Profile</a></li>
            @endif
          </ul>
        </div>
      </li>
    </ul>
  </nav>