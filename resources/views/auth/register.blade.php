<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Corona Admin</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{asset('corono/assets/vendors/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{asset('corono/assets/vendors/css/vendor.bundle.base.css') }}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="{{asset('corono/assets/css/style.css') }}">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="{{asset('corono/assets/images/favicon.png') }}" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type='text/javascript'>
      $(window).load(function(){
      $("#role").change(function() {
            console.log($("#role option:selected").val());
            if ($("#role option:selected").val() == 'Siswa') {
              $('#email').prop('hidden', false);
              $('#password').prop('hidden', false);
              $('#password-confirm').prop('hidden', false);
              $('#nama').prop('hidden', true);
              $('#telephone').prop('hidden', true);
              $('#alamat').prop('hidden', true);
              $('#nama_depan').prop('hidden', false);
              $('#nama_belakang').prop('hidden', false);
              $('#jenis_kelamin').prop('hidden', false);
              $('#nisn').prop('hidden', false);
              $('#nik').prop('hidden', false);
              $('#kelas').prop('hidden', false);
              $('#tgllahir').prop('hidden', false);
            } else {
              $('#email').prop('hidden', false);
              $('#password').prop('hidden', false);
              $('#password-confirm').prop('hidden', false);
              $('#nama').prop('hidden', false);
              $('#telephone').prop('hidden', false);
              $('#alamat').prop('hidden', false);
              $('#nama_depan').prop('hidden', true);
              $('#nama_belakang').prop('hidden', true);
              $('#jenis_kelamin').prop('hidden', true);
              $('#nisn').prop('hidden', true);
              $('#nik').prop('hidden', true);
              $('#kelas').prop('hidden', false);
              $('#tgllahir').prop('hidden', false);
            }
          });
      });
      </script>
  </head>
  <body>
    <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="row w-100 m-0">
          <div class="content-wrapper full-page-wrapper d-flex align-items-center auth login-bg">
            <div class="card col-lg-4 mx-auto">
              <div class="card-body px-5 py-5">
                <h3 class="card-title text-center mb-3">Register User</h3>
                <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                      <label>Role</label>
                      <select name="role" id="role" class="form-control text-white">
                          <option value="">Pilih</option>
                          <option value="Admin">Guru</option>
                          <option value="Siswa">Siswa</option>
                      </select>
                    </div>
                    @error('role')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                    @enderror
                  <div id="email" class="form-group">
                    <label id="email">Email</label>
                    <input name="email" type="email" id="email" class="form-control p_input text-white">
                  </div>
                  @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                  <div id="password" class="form-group">
                    <label id="password">Password</label>
                    <input name="password" type="password" id="password" class="form-control p_input">
                  </div>
                  @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                  <div id="password-confirm" class="form-group">
                    <label id="password_confirmation">Konfirmasi Password</label>
                    <input name="password_confirmation" id="password-confirm" type="password" class="form-control p_input">
                  </div>
                  @error('password-confirm')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                  <div id="nama" class="form-group">
                    <label id="nama">Nama</label>
                    <input name="nama" type="text" id="nama" class="form-control p_input text-white">
                  </div>
                  @error('nama')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
                @enderror
                  <div id="nama_depan" class="form-group">
                    <label id="nama_depan">Nama Depan</label>
                    <input name="nama_depan" type="text" id="nama_depan" class="form-control p_input text-white">
                  </div>
                  @error('nama_depan')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                  <div id="nama_belakang" class="form-group">
                    <label id="nama_belakang">Nama Belakang</label>
                    <input name="nama_belakang" type="text" id="nama_belakang" class="form-control p_input text-white">
                  </div>
                  @error('nama_belakang')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                  <div id="jenis_kelamin" class="form-group">
                    <label id="jenis_kelamin">Jenis Kelamin</label>
                    <select name="jenis_kelamin" class="form-control text-white" id="jenis_kelamin">
                        <option value="">Pilih</option>
                        <option value="Laki-laki">Laki-laki</option>
                        <option value="Perempuan">Perempuan</option>
                    </select>
                  </div>
                  @error('jenis_kelamin')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                  <div id="tgllahir" class="form-group">
                      <label id="tgllahir">Tanggal Lahir</label>
                      <input name="tgllahir" type="date" id="tgllahir" class="form-control p_input text-white">
                  </div>
                  @error('tgllahir')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                  <div id="kelas" class="form-group">
                      <label id="kelas">Kelas</label>
                      <select id="kelas"  name="kelas" value="" class="form-control text-white">
                        <option value="">Pilih</option>
                        <option value="TI">TI</option>
                        <option value="SI">SI</option>
                    </select>
                  </div>
                  @error('kelas')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                  <div id="nik" class="form-group">
                    <label id="nik">NIK</label>
                    <input name="nik" type="number" id="nik" class="form-control p_input text-white">
                  </div>
                  @error('nik')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                  <div id="nisn" class="form-group">
                    <label id="nisn">NISN</label>
                    <input name="nisn" type="number" id="nisn" class="form-control p_input text-white">
                  </div>
                  @error('nisn')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                  <div id="telephone" class="form-group">
                    <label id="telephone">Telephone</label>
                    <input name="telephone" type="number" id="telephone" class="form-control p_input text-white">
                  </div>
                  @error('telephone')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                  <div id="alamat" class="form-group">
                    <label id="alamat">Alamat</label>
                    <input type="text" name="alamat" id="alamat" class="form-control p_input text-white">
                  </div>
                  @error('alamat')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                  <div class="text-center">
                    <button type="submit" class="sign-up btn btn-primary btn-block enter-btn">Submit</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <!-- content-wrapper ends -->
        </div>
        <!-- row ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{asset('corono/assets/vendors/js/vendor.bundle.base.js') }}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{asset('corono/assets/js/off-canvas.js') }}"></script>
    <script src="{{asset('corono/assets/js/hoverable-collapse.js') }}"></script>
    <script src="{{asset('corono/assets/js/misc.js') }}"></script>
    <script src="{{asset('corono/assets/js/settings.js') }}"></script>
    <script src="{{asset('corono/assets/js/todolist.js') }}"></script>
    <!-- endinject -->
  </body>
</html>