@extends('layouts.master')
@push('search')
<ul class="navbar-nav w-100">
    <li class="nav-item w-100">
        <form class="nav-link mt-2 mt-md-0 d-none d-lg-flex search" method="GET" action="/siswa">
            <input name="cari" type="text" class="form-control" placeholder="Cari Nama Siswa">
        </form>
    </li>
  </ul>
@endpush
@section('content')
<div class="table-responsive">
  @if (auth()->user()->role == 'Superadmin')
  <a href="/siswa/input" class="btn btn-md btn-success float-right mt-2 ml-2">Data Siswa</a>
  @endif
    <table class="table table-striped">
      <thead>
        <tr>
          <th> No </th>
          <th> Nama Depan </th>
          <th> Nama Belakang </th>
          <th> Jenis Kelamin </th>
          <th> Tanggal Lahir </th>
          <th> Kelas </th>
          <th> Nisn </th>
          <th> Nik </th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        @forelse ($siswa as $key => $item)
          <tr>
            <td>{{ $key + 1 }}</td>
            <td>{{ $item->nama_depan }}</td>
            <td>{{ $item->nama_belakang }}</td>
            <td>{{ $item->jenis_kelamin }}</td>
            <td>{{ $item->tgllahir }}</td>
            <td>{{ $item->kelas }}</td>
            <td>{{ $item->nisn }}</td>
            <td>{{ $item->nik }}</td>
            <td>
              <form action="/siswa/{{ $item->id }}" method="POST">
                @csrf
                @method('delete')
                <a href="/siswa/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                <a href="/siswa/{{ $item->id }}/profile" class="btn btn-primary btn-sm">Profile</a>
                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
              </form>
            </td>
          </tr>
        @empty
          <tr>
            <td>Data tidak ada</td>
          </tr>
        @endforelse
      </tbody>
    </table>
  </div>
@endsection