@extends('layouts.master')
@push('search')
<ul class="navbar-nav w-100">
    <li class="nav-item w-100">
        <form class="nav-link mt-2 mt-md-0 d-none d-lg-flex search" method="GET" action="/siswa">
            <input name="cari" type="text" class="form-control" placeholder="Search products">
        </form>
    </li>
  </ul>
@endpush
@section('content')
<div class="content-wrapper">
    <div class="row">
      <div class="col-12 grid-margin stretch-card">
          <div class="card">
              <div class="card-body">
                <form class="forms-sample" action="/siswa/{{ $siswa->id }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="form-group row">
                    <label for="namadep" class="col-sm-3 col-form-label">Nama Depan</label>
                    <div class="col-sm-9">
                        <input name="nama_depan" value="{{ $siswa->nama_depan }}" type="text text-light" class="form-control text-white" id="namadep" placeholder="Nama Depan">
                    </div>
                    </div>
                    @error('nama_depan')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group row">
                    <label for="exampleInputEmail2" class="col-sm-3 col-form-label">Nama Belakang</label>
                    <div class="col-sm-9">
                        <input name="nama_belakang" value="{{ $siswa->nama_belakang }}" type="text" class="form-control text-white" id="exampleInputEmail2" placeholder="Nama Belakang">
                    </div>
                    </div>
                    @error('nama_belakang')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group row">
                    <label for="jenis_kelamin" class="col-sm-3 col-form-label">Jenis Kelamin</label>
                    <div class="col-sm-9">
                        <select name="jenis_kelamin" class="form-control text-white" id="jenis_kelamin">
                            <option value=""></option>
                            <option value="Laki-laki" @if($siswa->jenis_kelamin == 'Laki-laki') selected @endif>Laki-laki</option>
                            <option value="Perempuan" @if($siswa->jenis_kelamin == 'Perempuan') selected @endif>Perempuan</option>
                        </select>
                    </div>
                    @error('jenis_kelamin')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    </div>
                    <div class="form-group row">
                        <label for="tgglahir" class="col-sm-3 col-form-label">Tanggal Lahir</label>
                        <div class="col-sm-9">
                        <input name="tgllahir" value="{{ $siswa->tgllahir }}" type="date" class="form-control" id="tgglahir" placeholder="Tanggal Lahir">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="kelas" class="col-sm-3 col-form-label">kelas</label>
                        <div class="col-sm-9">
                            <select name="kelas" class="form-control text-white" id="jenis_kelamin">
                                <option value="">Pilih</option>
                                <option value="IT" @if($siswa->kelas == 'TI') selected @endif>IT</option>
                                <option value="SI" @if($siswa->kelas == 'SI') selected @endif>SI</option>
                            </select>
                        </div>
                    </div>
                    @error('kelas')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group row">
                        <label for="nisn" class="col-sm-3 col-form-label">NISN</label>
                        <div class="col-sm-9">
                        <input name="nisn" value="{{ $siswa->nisn }}" type="number" class="form-control text-white" id="nisn" placeholder="NISN">
                        </div>
                    </div>
                    @error('nisn')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group row">
                        <label for="nik" class="col-sm-3 col-form-label">NIK</label>
                        <div class="col-sm-9">
                        <input name="nik" value="{{ $siswa->nik }}" type="number" class="form-control text-white" id="nik" placeholder="NIK">
                        </div>
                    </div>
                    @error('nik')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group row">
                        <label for="profile" class="col-sm-3 col-form-label">Foto Profil</label>
                        <div class="col-sm-9">
                        <input name="avatar" value="{{ $siswa->avatar }}" type="file" class="form-control" id="profile">
                        </div>
                    </div>
                    @error('avatar')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button class="btn btn-dark">Cancel</button>
                </form>
              </div>
          </div>
            
      </div>
    </div>
</div>
@endsection