@extends('layouts.master')
@push('search')
<ul class="navbar-nav w-100">
    <li class="nav-item w-100">
        <form class="nav-link mt-2 mt-md-0 d-none d-lg-flex search" method="GET" action="/siswa">
            <input name="cari" type="text" class="form-control" placeholder="Search products">
        </form>
    </li>
  </ul>
@endpush
@section('content')

<div class="content-wrapper">
    <div class="row">
      <div class="col-12 grid-margin stretch-card">
          <div class="card">
              <div class="card-body">
                <form class="forms-sample" action="/siswa/store" enctype="multipart/form-data" method="POST">
                    @csrf
                    <div class="form-group row">
                    <label for="email" class="col-sm-3 col-form-label">Email</label>
                    <div class="col-sm-9">
                        <input name="email" value="" type="email text-light" class="form-control text-white" id="email" placeholder="Email">
                    </div>
                </div>
                    @error('email')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group row">
                        <label for="password" class="col-sm-3 col-form-label">Password</label>
                        <div class="col-sm-9">
                            <input name="password" value="" type="password" class="form-control text-white" id="email" placeholder="Password">
                        </div>
                    </div>
                        @error('password')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group row">
                            <label for="password_confirmation" class="col-sm-3 col-form-label">Konfirmasi Password</label>
                            <div class="col-sm-9">
                                <input name="password_confirmation" value="" type="password" class="form-control text-white" id="password_confirmation" placeholder="Konfirmasi Password">
                            </div>
                        </div>
                        @error('password_confirmation')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    <div class="form-group row">
                    <label for="namadep" class="col-sm-3 col-form-label">Nama Depan</label>
                    <div class="col-sm-9">
                        <input name="nama_depan" value="" type="text text-light" class="form-control text-white" id="namadep" placeholder="Nama Depan">
                    </div>
                    </div>
                    @error('nama_depan')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
    
                    <div class="form-group row">
                    <label for="exampleInputEmail2" class="col-sm-3 col-form-label">Nama Belakang</label>
                    <div class="col-sm-9">
                        <input name="nama_belakang" value="" type="text" class="form-control text-white" id="exampleInputEmail2" placeholder="Nama Belakang">
                    </div>
                    </div>
                    @error('nama_belakang')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group row">
                    <label for="jenis_kelamin" class="col-sm-3 col-form-label">Jenis Kelamin</label>
                    <div class="col-sm-9">
                        <select name="jenis_kelamin" value="" class="form-control text-white" id="jenis_kelamin">
                            <option value="">Pilih</option>
                            <option value="Laki-laki">Laki-laki</option>
                            <option value="Perempuan">Perempuan</option>
                        </select>
                    </div>
                    </div>
                    @error('jenis_kelamin')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group row">
                        <label for="tgglahir" class="col-sm-3 col-form-label">Tanggal Lahir</label>
                        <div class="col-sm-9">
                        <input name="tgllahir" value="" type="date" class="form-control" id="tgglahir" placeholder="Tanggal Lahir">
                        </div>
                    </div>
                    @error('tgllahir')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group row">
                        <label for="kelas" class="col-sm-3 col-form-label">kelas</label>
                        <div class="col-sm-9">
                            <select name="kelas" value="" class="form-control text-white" id="kelas">
                                <option value="">Pilih</option>
                                <option value="IT">IT</option>
                                <option value="SI">SI</option>
                            </select>
                        </div>
                    </div>
                    @error('kelas')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group row">
                        <label for="nisn" class="col-sm-3 col-form-label">NISN</label>
                        <div class="col-sm-9">
                        <input name="nisn" value="nisn" type="number" class="form-control text-white" id="nisn" placeholder="NISN">
                        </div>
                    </div>
                    @error('nisn')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group row">
                        <label for="nik" class="col-sm-3 col-form-label">NIK</label>
                        <div class="col-sm-9">
                        <input name="nik" value="nik" type="number" class="form-control text-white" id="nik" placeholder="NIK">
                        </div>
                    </div>
                    @error('nik')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group row">
                        <label for="profil" class="col-sm-3 col-form-label">Foto Profil</label>
                        <div class="col-sm-9">
                        <input name="avatar" type="file" class="form-control" id="profile">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button class="btn btn-dark">Cancel</button>
                </form>
              </div>
          </div>
            
      </div>
    </div>
</div>
@endsection