@extends('layouts.master')
@section('content')
<div class="row">
    <div class="col-lg-6 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Data Siswa</h4>
            
            <div class="owl-carousel owl-theme full-width owl-carousel-dash portfolio-carousel owl-loaded owl-drag" id="owl-carousel-basic">
            <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-1033px, 0px, 0px); transition: all 0.25s ease 0s; width: 2412px;"><div class="owl-item cloned" style="width: 334.5px; margin-right: 10px;"><div class="item">
                <img src="{{ $siswa->getAvatar()}}" class="img-sm-4" alt="Responsive image">
              </div></div>
             <div class="owl-nav"><button type="button" role="presentation" class="owl-prev"><i class="mdi mdi-chevron-left"></i></button><button type="button" role="presentation" class="owl-next"><i class="mdi mdi-chevron-right"></i></button></div><div class="owl-dots disabled"></div></div>
            <div class="d-flex py-4">
              <div class="preview-list w-100">
                <div class="preview-item p-0">
                  <div class="preview-thumbnail">
                  </div>
                  <div class="preview-item-content d-flex flex-grow">
                    <div class="flex-grow">
                      <div class="d-flex d-md-block d-xl-flex justify-content-between">
                        <h6 class="preview-subject text-center">{{ $siswa->nama_depan.' '.$siswa->nama_belakang}}</h6>
                      </div>
                      <p class="text-white">Tanggal Lahir : {{ $siswa->tgllahir }}</p>
                      <p class="text-white">Jenis Kelamin : {{ $siswa->jenis_kelamin }}</p>
                      <p class="text-white">Kelas         : {{ $siswa->kelas }}</p>
                      <p class="text-white">NISN : {{ $siswa->nisn }}</p>
                      <p class="text-white">NIK : {{ $siswa->nik }}</p>
                      </div>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
    </div>
    
    <div class="col-lg-6 grid-margin stretch-card float-right">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title float-center">Data Nilai</h4>
          <div class="btn-group pull-right">
            <button type="button" class="btn btn-raised btn-info btn-sm"  data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-plus-circle"></i>Nilai</button>
        </div>
          </p>
          <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>Kode</th>
                  <th>Nama</th>
                  <th>Semester</th>
                  <th>Nilai</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @forelse ($siswa->mapel as $mapel)
               <tr>
                  <td>{{ $mapel->kode }}</td>
                  <td>{{ $mapel->nama }}</td>
                  <td>{{ $mapel->semester }}</td>
                  <td>{{ $mapel->pivot->nilai }}</td>
                  @if (auth()->user()->role == 'Admin')
                    <td>
                      <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    </td>
                  </tr>
                  @endif
                  @if (auth()->user()->role == 'Superadmin')
                    <td>
                      <a class="btn btn-danger btn-md" href="/siswa/{{ $siswa->id }}/{{ $mapel->id }}/deletnilai">Delete</a>
                  </tr>
                  @endif
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Table Nilai</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form action="/siswa/{{ $siswa->id }}/addnilai" method="POST" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="form-group">
                  <label for="exampleFormControlSelect1">Mata Pelajaran</label>
                  <select name="mapel" class="form-control" id="exampleFormControlSelect1">
                    @foreach($siswa->mapel as $mp)
                      <option value="{{ $mp->id }}">{{ $mp->nama }}</option>
                    @endforeach
                  </select>
                </div>
              <div class="form-group{{ $errors->has('nilai') ? 'has-error' : '' }}">
                    <label for="exampleInputEmail1">Nilai</label>
                    <input name="nilai" value="{{ old('nilai') }}" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                  @if($errors->has('nilai'))
                     <span class="help-block">{{ $errors->first('nilai') }}</span>
                  @endif
              </div>
              
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save</button>
          </div>
      </form>    
    </div>
  </div>
</div>

@endsection