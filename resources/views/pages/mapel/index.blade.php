@extends('layouts.master')
@push('search')
<ul class="navbar-nav w-100">
    <li class="nav-item w-100">
        <form class="nav-link mt-2 mt-md-0 d-none d-lg-flex search" method="GET" action="/guru">
            <input name="cari" type="text" class="form-control" placeholder="Search products">
        </form>
    </li>
  </ul>
@endpush
@section('content')
<div class="table-responsive">
    @if (auth()->user()->role == 'Superadmin')
    <a href="/mapel/input" class="btn btn-md btn-success float-right mt-2 ml-2">Data Mapel</a>
    @endif
      <table class="table table-striped">
        <thead>
          <tr>
            <th> No </th>
            <th> Kode </th>
            <th> Nama </th>
            <th> Semester </th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($mapel as $key => $item)
            <tr>
                <td>{{ $key + 1 }}</td>
                <td>{{ $item->kode }}</td>
                <td>{{ $item->nama }}</td>
                <td>{{ $item->semester }}</td>
                <td>
                    <form action="/mapel/{{ $item->id }}" method="POST">
                      @csrf
                      @method('delete')
                      <a href="/mapel/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                      <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    </form>
                  </td>
                </tr>
              @empty
                <tr>
                  <td>Data tidak ada</td>
                </tr>
            @endforelse
        </tbody>
      </table>
    </div>
@endsection