@extends('layouts.master')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                 <div class="card-body">
                    <form class="forms-sample" action="/mapel/{{ $mapel->id }}" method="POST">
                        @method('put')
                        @csrf
                        <div class="form-group row">
                            <label for="kode" class="col-sm-3 col-form-label">Kode</label>
                            <div class="col-sm-9">
                                <input name="kode" value="{{ $mapel->kode }}" type="text" class="form-control text-white" id="email" placeholder="Kode">
                            </div>
                        </div>
                            @error('kode')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="form-group row">
                                <label for="nama" class="col-sm-3 col-form-label">Nama</label>
                                <div class="col-sm-9">
                                    <input name="nama" value="{{ $mapel->nama }}" type="text" class="form-control text-white" id="nama" placeholder="Nama">
                                </div>
                            </div>
                                @error('nama')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                        <div class="form-group row">
                            <label for="semester" class="col-sm-3 col-form-label">Semester</label>
                            <div class="col-sm-9">
                                <select name="semester" class="form-control text-white" id="semester">
                                    <option value="">Pilih</option>
                                    <option value="Genap" @if($mapel->semester == 'Genap') selected @endif>Genap</option>
                                    <option value="Ganjil"@if($mapel->semester == 'Ganjil') selected @endif>Ganjil</option>
                                </select>
                            </div>
                        </div>
                        @error('semester')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <button type="submit" class="btn btn-primary mr-2">Submit</button>
                        <button class="btn btn-dark">Cancel</button>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection