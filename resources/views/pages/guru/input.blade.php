@extends('layouts.master')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                 <div class="card-body">
                    <form class="forms-sample" action="/guru/store" enctype="multipart/form-data" method="POST">
                        @csrf
                        <div class="form-group row">
                            <label for="email" class="col-sm-3 col-form-label">Email</label>
                            <div class="col-sm-9">
                                <input name="email" value="" type="email text-light" class="form-control text-white" id="email" placeholder="Email">
                            </div>
                        </div>
                            @error('email')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="form-group row">
                                <label for="password" class="col-sm-3 col-form-label">Password</label>
                                <div class="col-sm-9">
                                    <input name="password" value="" type="password" class="form-control text-white" id="email" placeholder="Password">
                                </div>
                            </div>
                                @error('password')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <div class="form-group row">
                                    <label for="password_confirmation" class="col-sm-3 col-form-label">Konfirmasi Password</label>
                                    <div class="col-sm-9">
                                        <input name="password_confirmation" value="" type="password" class="form-control text-white" id="password_confirmation" placeholder="Konfirmasi Password">
                                    </div>
                                </div>
                                    @error('password_confirmation')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                        <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Nama</label>
                        <div class="col-sm-9">
                            <input name="nama" value="" type="text text-light" class="form-control text-white" id="namadep" placeholder="Nama">
                        </div>
                        </div>
                        @error('nama')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group row">
                            <label for="tgglahir" class="col-sm-3 col-form-label">Tanggal Lahir</label>
                            <div class="col-sm-9">
                            <input name="tgllahir" value="" type="date" class="form-control" id="tgglahir" placeholder="Tanggal Lahir">
                            </div>
                        </div>
                        @error('tgllahir')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group row">
                            <label for="kelas" class="col-sm-3 col-form-label">kelas</label>
                            <div class="col-sm-9">
                                <select name="kelas" value="" class="form-control text-white" id="kelas">
                                    <option value="">Pilih</option>
                                    <option value="IT">IT</option>
                                    <option value="SI">SI</option>
                                </select>
                            </div>
                        </div>
                        @error('kelas')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group row">
                            <label for="telephone" class="col-sm-3 col-form-label">Telephone</label>
                            <div class="col-sm-9">
                                <input name="telephone" value="" type="number text-light" class="form-control text-white" id="telephone" placeholder="Telephone">
                            </div>
                            </div>
                            @error('telephone')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="form-group row">
                                <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
                                <div class="col-sm-9">
                                    <textarea name="alamat" value="" type="text text-light" class="form-control text-white" id="namadep" placeholder="Alamat"></textarea>
                                </div>
                                </div>
                                @error('alamat')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                        <div class="form-group row">
                            <label for="profil" class="col-sm-3 col-form-label">Foto Profil</label>
                            <div class="col-sm-9">
                            <input name="avatar" type="file" class="form-control" id="profile">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary mr-2">Submit</button>
                        <button class="btn btn-dark">Cancel</button>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection