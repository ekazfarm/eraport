@extends('layouts.master')
@push('search')
<ul class="navbar-nav w-100">
    <li class="nav-item w-100">
        <form class="nav-link mt-2 mt-md-0 d-none d-lg-flex search" method="GET" action="/siswa">
            <input name="cari" type="text" class="form-control" placeholder="Search products">
        </form>
    </li>
  </ul>
@endpush
@section('content')
<div class="content-wrapper">
    <div class="row">
      <div class="col-12 grid-margin stretch-card">
          <div class="card">
              <div class="card-body">
                <form class="forms-sample" action="/guru/{{ $guru->id }}" method="POST" enctype="multipart/form-data">
                    @method('put')
                    @csrf
                    <input name="user_id" value="{{ $guru->user_id }}" type="hidden">
                    <div class="form-group row">
                    <label for="nama" class="col-sm-3 col-form-label">Nama</label>
                    <div class="col-sm-9">
                        <input name="nama" value="{{ $guru->nama }}" type="text text-light" class="form-control text-white" id="nama" placeholder="Nama">
                    </div>
                    </div>
                    @error('nama')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group row">
                        <label for="tgglahir" class="col-sm-3 col-form-label">Tanggal Lahir</label>
                        <div class="col-sm-9">
                        <input name="tgllahir" value="{{ $guru->tgllahir }}" type="date" class="form-control" id="tgglahir" placeholder="Tanggal Lahir">
                        </div>
                    </div>
                    @error('tgllahir')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group row">
                        <label for="kelas" class="col-sm-3 col-form-label">Wali Kelas</label>
                        <div class="col-sm-9">
                            <select name="kelas" class="form-control text-white" id="jenis_kelamin">
                                <option value="">Pilih</option>
                                <option value="IT" @if($guru->kelas == 'TI') selected @endif>IT</option>
                                <option value="SI" @if($guru->kelas == 'SI') selected @endif>SI</option>
                            </select>
                        </div>
                    </div>
                    @error('kelas')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group row">
                        <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
                        <div class="col-sm-9">
                            <textarea name="alamat" class="form-control" id="exampleTextarea1" rows="4">{{ $guru->alamat }}</textarea>
                        </div>
                    </div>
                        @error('alamat')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="form-group row">
                            <label for="telephone" class="col-sm-3 col-form-label">Telephone</label>
                            <div class="col-sm-9">
                                <input name="telephone" value="{{ $guru->telephone }}" type="number" class="form-control" id="telephone" placeholder="Telephone">
                            </div>
                        </div>
                            @error('telephone')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                    <div class="form-group row">
                        <label for="profil" class="col-sm-3 col-form-label">Foto Profil</label>
                        <div class="col-sm-9">
                        <input name="avatar" type="file" class="form-control" id="profile">
                        </div>
                    </div>
                            @error('avatar')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button class="btn btn-dark">Cancel</button>
                </form>
              </div>
          </div>
            
      </div>
    </div>
</div>
@endsection