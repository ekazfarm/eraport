@extends('layouts.master')
@push('search')
<ul class="navbar-nav w-100">
    <li class="nav-item w-100">
        <form class="nav-link mt-2 mt-md-0 d-none d-lg-flex search" method="GET" action="/guru">
            <input name="cari" type="text" class="form-control" placeholder="Search products">
        </form>
    </li>
  </ul>
@endpush
@section('content')
<div class="table-responsive">
    <a href="/guru/input" class="btn btn-md btn-success float-right mt-2 ml-2">Data Guru</a>
      <table class="table table-striped">
        <thead>
          <tr>
            <th> No </th>
            <th> Nama </th>
            <th> Tanggal Lahir </th>
            <th> Wali Kelas</th>
            <th> Telephones </th>
            <th> Alamat </th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($guru as $key => $item)
            <tr>
              <td>{{ $key + 1 }}</td>
              <td>{{ $item->nama }}</td>
              <td>{{ $item->tgllahir }}</td>
              <td>{{ $item->kelas }}</td>
              <td>{{ $item->telephone }}</td>
              <td>{{ $item->alamat }}</td>
              <td>
                <form action="/guru/{{ $item->id }}" method="POST">
                  @csrf
                  @method('delete')
                  <a href="/guru/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                  <a href="/guru/{{ $item->id }}/profile" class="btn btn-primary btn-sm">Profile</a>
                  <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
              </td>
            </tr>
          @empty
            <tr>
              <td>Data tidak ada</td>
            </tr>
          @endforelse
        </tbody>
      </table>
    </div>
@endsection