<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = 'siswa';
    protected $fillable = ['nama_depan', 'nama_belakang', 'jenis_kelamin', 'tgllahir', 'kelas', 'nisn', 'nik','user_id', 'avatar'];

    public function getAvatar()
    {
        if(!$this->avatar){
            return asset('/images/default.png');
        }
        return asset('images/'.$this->avatar);
    }

    public function nama_lengkap()
    {
        return $this->nama_depan.' '.$this->nama_belakang;
    }

    public function mapel()
    {
        return $this->belongsToMany(Mapel::class)->withPivot(['nilai'])->withTimeStamps();
    }


}
