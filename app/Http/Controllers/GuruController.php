<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Guru;
use App\User;

use Illuminate\Http\Request;

class GuruController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('cari')){
            $guru = \App\Siswa::where('nama', 'LIKE', '%'.$request->cari.'%')->get();
        }else{
            $guru = DB::table('guru')->get();
        }
        return view('pages.guru.index', compact('guru'));
    }

    public function input()
    {
        return view('pages.guru.input');
    }

    public function store(Request $request)
    {
    //     $request->validate([
    //         'nama' => 'required',
    //         'tgllahir' => 'required',
    //         'kelas' => 'required',
    //         'telephone' => 'required|numeric',
    //         'alamat' => 'required',
    //         'avatar' => 'required|mimes:jpeg,jpg,png'
            
    //     ],
    //     [
    //         'nama.required' => 'Nama belum diisi',
    //         'tgllahir.required' => 'Tanggal lahir belum diisi',
    //         'kelas.required' => 'Kelas ajar belum diisi',
    //         'telephone.required' => 'Nomer telephone belum diisi',
    //         'alamat.required' => 'alamat belum diisi',
    //         'avatar.required' => 'File salah atau belum diisi'
            
    //     ]
    // );

            $user = new User;
            $user->role = 'Admin';
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->remember_token = Str::random(12);
            $user->save();

            $fileName = time().'.'.$request->avatar->extension();

            $guru = new Guru;

            $guru->nama = $request->nama;
            $guru->tgllahir = $request->tgllahir;
            $guru->kelas = $request->kelas;
            $guru->telephone = $request->telephone;
            $guru->alamat = $request->alamat;
            $guru->avatar = $fileName;
            $guru->user_id = $user->id;

            $guru->save();

            $request->avatar->move(public_path('images'), $fileName);

            return redirect('/guru');

    }

    public function edit($id)
    {
        $guru = DB::table('guru')->where('id', $id)->first();
        return view('pages.guru.edit', compact('guru'));
    }

    public function update(Request $request, $id)
    {
        
        $fileName = time().'.'.$request->avatar->extension();
        $guru = Guru::findorfail($id);
        $guru->update([
            $guru->nama = $request->nama,
            $guru->tgllahir = $request->tgllahir,
            $guru->kelas = $request->kelas,
            $guru->telephone = $request->telephone,
            $guru->alamat = $request->alamat,
            $guru->avatar = $fileName,
            $guru->save(),
            $request->avatar->move(public_path('images'), $fileName)
    ]);
        return redirect('/guru')->with('sukses', 'Data berhasil di update');
    }

    public function destroy($id)
    {   
        
        DB::table('guru')->where('id', '=', $id)->delete();
        return redirect('/guru');
    }

   
}
