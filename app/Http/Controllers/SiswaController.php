<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Http\Controllers\Schema;

use Illuminate\Http\Request;
use App\Siswa;
use App\User;
use App\Mapel;

class SiswaController extends Controller
{
    public function siswa(Request $request)
    {
        if($request->has('cari')){
            $siswa = \App\Siswa::where('nama_depan', 'LIKE', '%'.$request->cari.'%')->get();
        }else{
            $siswa = DB::table('siswa')->get();
        }
        return view('pages.siswa.index', compact('siswa'));
    }

    public function input()
    {   
        return view('pages.siswa.input');
    }

    public function store(Request $request)
    {
            $request->validate([
                'nama_depan' => 'required',
                'nama_belakang' => 'required',
                'jenis_kelamin' => 'required',
                'tgllahir' => 'required',
                'kelas' => 'required',
                'nisn' => 'required|numeric',
                'nik' => 'required|numeric'
            ],
            [
                'nama_depan.required' => 'Nama depan belum diisi',
                'nama_belakang.required' => 'Nama belakang belum diisi',
                'jenis_kelamin.required' => 'Jenis kelamin belum diisi',
                'tgllahir.required' => 'Tanggal lahir belum diiisi',
                'kelas.required' => 'Kelas belum diisi',
                'nisn.required' => 'Nomer NISN belum diisi',
                'nik.required' => 'Nomer NIK belum diiisi'
            ]
        );
            $user = new User;
            $user->role = 'Siswa';
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->remember_token = Str::random(12);
            $user->save();

            $fileName = time().'.'.$request->avatar->extension();

            $siswa = new Siswa;

            $siswa->nama_depan = $request->nama_depan;
            $siswa->nama_belakang = $request->nama_belakang;
            $siswa->jenis_kelamin = $request->jenis_kelamin;
            $siswa->tgllahir = $request->tgllahir;
            $siswa->kelas = $request->kelas;
            $siswa->nisn = $request->nisn;
            $siswa->nik = $request->nik;
            $siswa->avatar = $fileName;
            $siswa->user_id = $user->id;

            $siswa->save();

            $request->avatar->move(public_path('images'), $fileName);

            return redirect('/siswa');

            // $request->request->add(['user_id' => $user->id]);
            // $siswa = Siswa::create($request->all());
            // if($request->hasfile('avatar')){
            //     $request->file('avatar')->move('images/',$request->file('avatar')->getClientOriginalName());
            //     $siswa->avatar = $request->file('avatar')->getClientOriginalName();
            //     $siswa->save();
            // }
            // return redirect('/siswa');
    }

    public function edit($id)
    {
        $siswa = DB::table('siswa')->where('id', $id)->first();
        return view('pages.siswa.edit', compact('siswa'));
    }


    public function update(Request $request,$id)
    {
        $request->validate([
                'nama_depan' => 'required',
                'nama_belakang' => 'required',
                'jenis_kelamin' => 'required',
                'tgllahir' => 'required',
                'kelas' => 'required',
                'nisn' => 'required|numeric',
                'nik' => 'required|numeric'
            ],
            [
                'nama_depan.required' => 'Nama depan belum diisi',
                'nama_belakang.required' => 'Nama belakang belum diisi',
                'jenis_kelamin.required' => 'Jenis kelamin belum diisi',
                'tgllahir.required' => 'Tanggal lahir belum diiisi',
                'kelas.required' => 'Kelas belum diisi',
                'nisn.required' => 'Nomer NISN belum diisi atau kesalahan dalam pengisian',
                'nik.required' => 'Nomer NIK belum diiisi atau kelasahan dalam pengisian'
            ]
        );

            $fileName = time().'.'.$request->avatar->extension();
            $siswa = Siswa::findorfail($id);
            $siswa->update([
                $siswa->nama_depan = $request->nama_depan,
                $siswa->nama_belakang = $request->nama_belakang,
                $siswa->jenis_kelamin = $request->jenis_kelamin,
                $siswa->tgllahir = $request->tgllahir,
                $siswa->kelas = $request->kelas,
                $siswa->nisn = $request->nisn,
                $siswa->nik = $request->nik,
                $siswa->avatar = $fileName,
                $siswa->save(),
                $request->avatar->move(public_path('images'), $fileName)
        ]);
        return redirect('/siswa')->with('sukses', 'Data berhasil di update');
    }

    public function destroy($id)
    {

        DB::table('siswa')->where('id', '=', $id)->delete();
        return redirect('/siswa');
    }

    public function profile($id)
    {
        $mapel = Mapel::findorfail($id);
        $siswa = Siswa::findorfail($id);
        return view('pages.siswa.profile',['siswa'=>$siswa,'mapel'=>$mapel]);
        
        // dd($siswa->mapel->guru);

    }

    public function deletnilai(Siswa $siswa,$idmapel)
    {
        $siswa->mapel()->detach($idmapel);
        return redirect()->back()->with('sukses', 'Data nilai berhasil dihapus');
    }

   
}
