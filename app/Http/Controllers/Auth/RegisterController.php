<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Siswa;
use App\Guru;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            // 'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            // 'password' => ['required', 'string', 'min:4', 'confirmed'],
            // 'role' => ['required']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // dd($data->all());

            if($data['role'] == 'Admin') {
            $user = User::create([
                'email' => $data['email'],
                'role' => $data['role'],
                'password' => Hash::make($data['password']),
            ]);
    
            Guru::create([
                'nama' => $data['nama'],
                'tgllahir' => $data['tgllahir'],
                'kelas' => $data['kelas'],
                'telephone' => $data['telephone'],
                'alamat' => $data['alamat'],
                'user_id' => $user->id
            ]);
            
            }
         
             else{
                $user = User::create([
                    'email' => $data['email'],
                    'role' => $data['role'],
                    'password' => Hash::make($data['password']),
                ]);
        
                Siswa::create([
                    'nama_depan' => $data['nama_depan'],
                    'nama_belakang' => $data['nama_belakang'],
                    'jenis_kelamin' => $data['jenis_kelamin'],
                    'tgllahir' => $data['tgllahir'],
                    'kelas' => $data['kelas'],
                    'nisn' => $data['nisn'],
                    'nik' => $data['nik'],
                    'user_id' => $user->id
                ]);
    
            }

        return $user;
            
    }
}
