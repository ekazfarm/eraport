<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Mapel;

use Illuminate\Http\Request;

class MapelController extends Controller
{
    public function index()
    {
        $mapel = DB::table('mapel')->get();
        return view('pages.mapel.index', compact('mapel'));
    }

    public function input()
    {
        return view('pages.mapel.input');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'kode' => 'required|min:3',
            'nama' => 'required|min:2',
            'semester' => 'required',
        ]);

        $data_mapel = Mapel::create($request->all());
        return redirect('/mapel')->with('sukses', 'Data berhasil di input');
    }

    public function edit($id)
    {
        $mapel = DB::table('mapel')->where('id', $id)->first();
        return view('pages.mapel.edit', compact('mapel'));
    }

    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'kode' => 'required|min:3',
            'nama' => 'required|min:2',
            'semester' => 'required',
        ],
        [
            'kode.required' => 'Kode belum diisi',
            'nama.required' => 'Nama matapelajaran belum diisi',
            'semester.required' => 'Semester belum diiisi'
        ]);

        DB::table('mapel')
        ->where('id', $id )
        ->update(
            [
                'kode' => $request['kode'],
                 'nama' => $request['nama'],
                 'semester' => $request['semester']
          ]);

        return redirect('/mapel');
    }

    public function destroy($id)
    {
        DB::table('mapel')->where('id', '=', $id)->delete();

        return redirect('/mapel');
    }
}
