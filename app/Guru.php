<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    protected $table = 'guru';
    protected $fillable = ['nama', 'tgllahir', 'kelas', 'telephone', 'alamat', 'user_id','avatar'];

    public function getAvatar()
    {
        if(!$this->avatar){
            return asset('/images/default.png');
        }
        return asset('images/'.$this->avatar);
    }
   
}
    

